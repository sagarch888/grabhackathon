app.controller('paymentCrtl', function ($scope, $route, PaymentService, toastr) {
	$scope.today = new Date();

	// Called on page init.
	$scope.initPage = function () {
		$scope.showOtpBtn = true;
		$scope.showReceiveBtn = false;
		$scope.isLoading = false;
	}

	$scope.requestOTP = function () {
		if ($scope.mobilenumber != null && $scope.amount != null) {
			$scope.isLoading = true;
			$scope.showOtpBtn = false;
			PaymentService.requestOTP($scope.mobilenumber, $scope.amount).then(function successCallback(response) {
				$scope.isLoading = false;
				if (response.data) {
					toastr.success(response.data.result, 'Otp Request sent successfully!');
					$scope.showReceiveBtn = true;
					//$route.reload();
				} else {
					// @TODO: Handle the error properly instead of alert.
					toastr.warning(response.data.message, 'Request Unsuccessfully!');
				}
			}, function errorCallback(response) {
				$scope.isLoading = false;
				$scope.showOtpBtn = true;
				// @TODO: Handle the error properly instead of alert.
				toastr.error(response.data.message || 'Error@Processing the action', 'Error !!');
				$route.reload();
			});
		} else {
			toastr.warning('Please enter Mobile Number or amount.');
		}
	}

	$scope.receiveAmt = function () {
		if ($scope.mobilenumber != null && $scope.amount != null && $scope.otp != null) {
			$scope.showReceiveBtn = false;
			$scope.isLoading = true;
			PaymentService.receiveAmt($scope.mobilenumber, $scope.amount, $scope.otp).then(function successCallback(response) {
				$scope.isLoading = false;
				debugger;
				if (response.data.success) {
					toastr.success(response.data.data, 'Amount received successfully!');
					$route.reload();
				} else {
					// @TODO: Handle the error properly instead of alert.
					toastr.warning(response.data.message, 'Transaction Unsuccessfully!');
					$route.reload();
				}
			}, function errorCallback(response) {
				$scope.isLoading = false;
				// @TODO: Handle the error properly instead of alert.
				toastr.error(response.data.message || 'Error@Processing the action.', 'Error !!');
				$route.reload();
			});
		} else {
			toastr.warning('Please enter Mobile Number and OTP');
		}
	}

	$scope.initPage();
});